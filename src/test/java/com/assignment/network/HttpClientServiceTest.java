package com.assignment.network;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.assignment.exceptions.RemoteServiceExecutionException;
import com.assignment.model.ResponseCodeAndBody;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringRunner.class)
@PrepareForTest({ URL.class, HttpURLConnection.class })
@SpringBootTest
public class HttpClientServiceTest {

	@Autowired
	private HttpClientService httpClientService;

	@Test
	public void get() throws Exception {
		String url = "http://www.google.com";
		final ResponseCodeAndBody responseCodeAndBody = httpClientService.get(url);
		assertEquals(200, responseCodeAndBody.getCode());
	}

	@Test
	public void getException() throws Exception {
		String url = "http://api.weatherapi.com/v1/forecast.json?key=abc&q=10001&days=2";
		assertThrows(RemoteServiceExecutionException.class, () -> {
			httpClientService.get(url);
		});
	}

}