package com.assignment.network;

import static org.junit.Assert.assertEquals;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringRunner.class)
@PrepareForTest({ URL.class, HttpURLConnection.class })
@SpringBootTest
public class ApiBuilderTest {

	String validUrl = "http://api.weatherapi.com/v1/forecast.json?key=216eacf0d9fa46518c3163509213001&q=10001&days=2";

	@Test
	public void get() throws Exception {
		assertEquals(validUrl, ApiBuilder.getRequestURL("10001"));
	}

}