package com.assignment.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.assignment.services.WeatherTempratureService;
import com.assignment.services.impl.WeatherPredictionServiceImpl;

@RunWith(SpringRunner.class)
@WebMvcTest(value = WeatherPredictionController.class)
public class WeatherPredictionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	WeatherPredictionServiceImpl weatherPredictionService;

	@MockBean
	WeatherTempratureService tempratureService;

	@Test
	public void weatherPrediction() throws Exception {

		mockMvc.perform(get("/coolesthour/get")
				.param("zipcode", "10001")
		           .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
		           .andExpect(status().isOk());
	}
	
	@Test
	public void weatherPredictionWrongZipcode() throws Exception {

		mockMvc.perform(get("/coolesthour/get")
				.param("zipcode", "10")
		           .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
		           .andExpect(jsonPath("code", is(400)));
	}

}