package com.assignment.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.assignment.exceptions.WeatherPredictionReportNotFoundException;
import com.assignment.model.Forecastday;
import com.assignment.model.Hour;
import com.assignment.model.ResponseCodeAndBody;
import com.assignment.network.ApiBuilder;
import com.assignment.network.HttpClientService;
import com.assignment.services.WeatherPredictionService;
import com.assignment.services.impl.WeatherTempratureServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class WeatherTempratureServiceImplTest {

	private static StringBuilder jsonResponse = new StringBuilder("{\n" + "    \"location\": {\n"
			+ "        \"name\": \"New York\",\n" + "        \"region\": \"New York\",\n"
			+ "        \"country\": \"USA\",\n" + "        \"lat\": 40.75,\n" + "        \"lon\": -73.99,\n"
			+ "        \"tz_id\": \"America/New_York\",\n" + "        \"localtime_epoch\": 1612024649,\n"
			+ "        \"localtime\": \"2021-01-30 11:37\"\n" + "    },\n" + "    \"forecast\": {\n"
			+ "        \"forecastday\": [\n" + "            {\n");

	@Autowired
	private WeatherTempratureServiceImpl weatherTempratureServiceImpl;

	@Autowired
	private WeatherPredictionService weatherPredictionService;

	@MockBean
	private HttpClientService httpClientService;

	private static String validUSZipCode = "10001";

	@BeforeAll
	public static void setup() {

		String dayStr = "\"date\":\"" + LocalDate.now().plusDays(1).toString() + "\",\n" + "\"hour\": [\n";
		jsonResponse.append(dayStr);
		for (int i = 0; i < 24; i++) {
			String hourString = "{\n" + "\"time_epoch\": 1611982800,\n" + "\"time\": \""
					+ LocalDate.now().plusDays(1).atStartOfDay().plusHours(i).toString() + "\",\n" + "\"temp_c\": " + i
					+ ",\n" + "\"temp_f\": 18.3\n" + "}\n";

			jsonResponse.append(hourString);
			if (i != 23)
				jsonResponse.append(",");
		}

		jsonResponse.append("]\n" + "} \n" + "]\n" + "}\n" + "}");

	}

	@Test
	public void calculateAndPrintCoolestTemprature() throws IOException {
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();
		responseCodeAndBody.setCode(200);
		responseCodeAndBody.setBody(jsonResponse.toString());
		when(httpClientService.get(ApiBuilder.getRequestURL(validUSZipCode))).thenReturn(responseCodeAndBody);
		List<Forecastday> weatherPredictionReport = weatherPredictionService
				.getNextDayWeatherPrediction(validUSZipCode);
		ResponseCodeAndBody response = weatherTempratureServiceImpl
				.calculateAndPrintCoolestTemprature(weatherPredictionReport);
		assertEquals(200, response.getCode());
	}

	@Test
	public void getMinTempOfHour() throws IOException {
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();
		responseCodeAndBody.setCode(200);
		responseCodeAndBody.setBody(jsonResponse.toString());
		when(httpClientService.get(ApiBuilder.getRequestURL(validUSZipCode))).thenReturn(responseCodeAndBody);
		List<Forecastday> weatherPredictionReport = weatherPredictionService
				.getNextDayWeatherPrediction(validUSZipCode);
		Optional<Hour> coolestHour = weatherTempratureServiceImpl
				.getMinTempOfHour(weatherPredictionReport.get(0).getHours());
		assertTrue(!coolestHour.isEmpty());
		assertEquals(0., coolestHour.get().getTemp_c());

	}

	@Test
	public void weatherDataNotFoundTest() throws IOException {
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();
		responseCodeAndBody.setCode(200);
		responseCodeAndBody.setBody(jsonResponse.toString());
		when(httpClientService.get(ApiBuilder.getRequestURL(validUSZipCode))).thenReturn(responseCodeAndBody);
		List<Forecastday> weatherPredictionReport = weatherPredictionService
				.getNextDayWeatherPrediction(validUSZipCode);
		weatherPredictionReport.get(0).setHours(new ArrayList<>());
		assertThrows(WeatherPredictionReportNotFoundException.class, () -> {
			weatherTempratureServiceImpl.calculateAndPrintCoolestTemprature(weatherPredictionReport);
		});
	}

}