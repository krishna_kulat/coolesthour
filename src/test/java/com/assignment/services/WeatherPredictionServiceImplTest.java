package com.assignment.services;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import com.assignment.exceptions.RemoteServiceExecutionException;
import com.assignment.exceptions.WeatherPredictionReportNotFoundException;
import com.assignment.model.Forecastday;
import com.assignment.model.ResponseCodeAndBody;
import com.assignment.network.ApiBuilder;
import com.assignment.network.HttpClientService;
import com.assignment.services.WeatherPredictionService;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class WeatherPredictionServiceImplTest {

	private static StringBuilder jsonResponse = new StringBuilder("{\n" + "    \"location\": {\n"
			+ "        \"name\": \"New York\",\n" + "        \"region\": \"New York\",\n"
			+ "        \"country\": \"USA\",\n" + "        \"lat\": 40.75,\n" + "        \"lon\": -73.99,\n"
			+ "        \"tz_id\": \"America/New_York\",\n" + "        \"localtime_epoch\": 1612024649,\n"
			+ "        \"localtime\": \"2021-01-30 11:37\"\n" + "    },\n" + "    \"forecast\": {\n"
			+ "        \"forecastday\": [\n" + "            {\n");

	@Autowired
	private WeatherPredictionService weatherPredictionService;
	@MockBean
	private HttpClientService httpClientService;

	private static String validUSZipCode = "10001";
	private static String inValidUSZipCode = "00000";

	@BeforeAll
	public static void setup() {

		String dayStr = "\"date\":\"" + LocalDate.now().plusDays(1).toString() + "\",\n" + "\"hour\": [\n";
		jsonResponse.append(dayStr);
		for (int i = 0; i < 24; i++) {
			String hourString = "{\n" + "\"time_epoch\": 1611982800,\n" + "\"time\": \""
					+ LocalDate.now().plusDays(1).atStartOfDay().plusHours(i).toString() + "\",\n" + "\"temp_c\": " + i
					+ ",\n" + "\"temp_f\": 18.3\n" + "}\n";

			jsonResponse.append(hourString);
			if (i != 23)
				jsonResponse.append(",");
		}

		jsonResponse.append("]\n" + "} \n" + "]\n" + "}\n" + "}");
	}

	@Test
	public void getNextDayWeatherPrediction() throws IOException {
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();
		responseCodeAndBody.setCode(200);
		responseCodeAndBody.setBody(jsonResponse.toString());
		when(httpClientService.get(ApiBuilder.getRequestURL(validUSZipCode))).thenReturn(responseCodeAndBody);
		List<Forecastday> forcastedDays = weatherPredictionService.getNextDayWeatherPrediction(validUSZipCode);
		assertFalse(forcastedDays.isEmpty());
		assertEquals(24, forcastedDays.get(0).getHours().size());

	}

	@Test
	public void remoteServiceExceptionTest() throws IOException {
		when(httpClientService.get(ApiBuilder.getRequestURL(inValidUSZipCode)))
				.thenThrow(RemoteServiceExecutionException.class);
		assertThrows(RemoteServiceExecutionException.class, () -> {
			weatherPredictionService.getNextDayWeatherPrediction(inValidUSZipCode);
		});

	}

	@Test
	public void weatherDataNotFoundTest() throws IOException {
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();
		responseCodeAndBody.setCode(200);
		responseCodeAndBody.setBody(null);
		when(httpClientService.get(ApiBuilder.getRequestURL(validUSZipCode))).thenReturn(responseCodeAndBody);
		assertThrows(WeatherPredictionReportNotFoundException.class, () -> {
			weatherPredictionService.getNextDayWeatherPrediction(validUSZipCode);
		});

	}
}