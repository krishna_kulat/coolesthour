package com.assignment.controller;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.model.Forecastday;
import com.assignment.model.ResponseCodeAndBody;
import com.assignment.services.WeatherTempratureService;
import com.assignment.services.impl.WeatherPredictionServiceImpl;

/**
 * This is the controller class for coolest hour app
 */ 

@RestController
public class WeatherPredictionController {

	
	
	@Autowired
	WeatherPredictionServiceImpl weatherPredictionService;

	@Autowired
	WeatherTempratureService tempratureService;

	/**
	 * It takes zipcode as input and get tomarrow weather prediction data and calculate coolest hour using different services
	 * @param zipCode
	 * @return coolest hour
	 */
	@GetMapping(value = "/coolesthour/get")
	public ResponseCodeAndBody weatherPrediction(@RequestParam("zipcode") String zipCode) {
		ResponseCodeAndBody response = new ResponseCodeAndBody();
		String regex = "^[0-9]{5}(?:-[0-9]{4})?$";
		Pattern pattern = Pattern.compile(regex);

		if (pattern.matcher(zipCode).matches()) {
			List<Forecastday> tomarrowsForcastList = weatherPredictionService.getNextDayWeatherPrediction(zipCode);
			response = tempratureService.calculateAndPrintCoolestTemprature(tomarrowsForcastList);

		} else {
			response.setCode(400);
			response.setBody("Invalid zipcode!");
		}
		return response;

	}

	 

}
