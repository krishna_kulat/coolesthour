package com.assignment.network;

/**
 * 
 * @author krishnakulat
 * This class is used to build api url to request weather data
 *
 */
public class ApiBuilder {
	private ApiBuilder() {
	}

	private static String baseUrl = "http://api.weatherapi.com/v1/forecast.json?key=216eacf0d9fa46518c3163509213001&q={zipcode}&days=2";

	public static String getRequestURL(String zipCode) {
		return baseUrl.replace("{zipcode}", zipCode);
	}
}
