package com.assignment.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.assignment.exceptions.RemoteServiceExecutionException;
import com.assignment.model.ResponseCodeAndBody;

/**
 * 
 * @author krishnakulat
 * This class is used to perform http request
 */
@Service
public class HttpClientService {
	
	/**
	 * This method reutrn 200 code if successful else throw Exception
	 * @param urlString
	 * @return ResponseCodeAndBody
	 * @throws IOException
	 */
	public ResponseCodeAndBody get(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();
		responseCodeAndBody.setCode(conn.getResponseCode());
		if (conn.getResponseCode() == 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			StringBuilder body = new StringBuilder();
			String output;
			while ((output = br.readLine()) != null) {
				body.append(output);
			}
			conn.disconnect();

			responseCodeAndBody.setBody(body.toString());
		} else {
			throw new RemoteServiceExecutionException(
					"The service at " + urlString + " returned with code " + conn.getResponseCode());
		}
		return responseCodeAndBody;
	}
}