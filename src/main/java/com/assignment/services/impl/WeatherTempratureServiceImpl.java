package com.assignment.services.impl;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.assignment.exceptions.WeatherPredictionReportNotFoundException;
import com.assignment.model.Forecastday;
import com.assignment.model.Hour;
import com.assignment.model.ResponseCodeAndBody;
import com.assignment.services.WeatherTempratureService;

/**
 * 
 * @author krishnakulat
 * This class is service implementation of WeatherTempratureService class.
 * It calculate coolest hour of day and print the tempratures.
 */
@Service
public class WeatherTempratureServiceImpl implements WeatherTempratureService {

	private static final Logger logger = LogManager.getLogger(WeatherTempratureServiceImpl.class);

	
	/**
	 * It print the hourly tempratures and coolest hour of day.
	 * If no hourly data present then it throw WeatherPredictionReportNotFoundException
	 * @param forecastDays
	 * @return coolest hour
	 * @throws WeatherPredictionReportNotFoundException
	 */
	@Override
	public ResponseCodeAndBody calculateAndPrintCoolestTemprature(List<Forecastday> forecastDays) {
		ResponseCodeAndBody responseCodeAndBody = new ResponseCodeAndBody();

		for (Forecastday forecastday : forecastDays) {
 
			if (!forecastday.getHours().isEmpty()) {
				logger.info("Hourly Forcast Temprature - : ");
				forecastday.getHours().forEach(
						hour -> logger.info("Date/Time : {}, Temprature :{}", hour.getTime(), hour.getTemp_c()));

				getMinTempOfHour(forecastday.getHours()).ifPresent(coolestHour -> {
					String outputString = "coolest hour of day: " + coolestHour.getTime()
							+ " and coolest temprature is: " + coolestHour.getTemp_c() + "'C";
					logger.info(outputString);
					responseCodeAndBody.setCode(200);
					responseCodeAndBody.setBody(outputString);
				});

			} else {
				throw new WeatherPredictionReportNotFoundException("Currently temprature is not found");
			}
		}

		return responseCodeAndBody;
	}

	
	/**
	 * It get input as list of hours and find min temprature and return coolest hour
	 * @param hours
	 * @return hour
	 */
	public Optional<Hour> getMinTempOfHour(List<Hour> hours) {
		return hours.stream().min(Comparator.comparing(Hour::getTemp_c));
	}

}
