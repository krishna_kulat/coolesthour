package com.assignment.services.impl;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.exceptions.WeatherPredictionReportNotFoundException;
import com.assignment.model.Forecastday;
import com.assignment.model.ResponseCodeAndBody;
import com.assignment.model.WeatherReportModel;
import com.assignment.network.ApiBuilder;
import com.assignment.network.HttpClientService;
import com.assignment.services.WeatherPredictionService;
import com.google.gson.Gson;

/**
 * 
 * @author krishnakulat This class is service implementation of
 *         WeatherPredictionService It call http request to get weather
 *         prediction data and filter with tomarrow date
 */
@Service
public class WeatherPredictionServiceImpl implements WeatherPredictionService {

	private final HttpClientService httpClientService;
	private static final Logger logger = LogManager.getLogger(WeatherPredictionServiceImpl.class);

	@Autowired
	public WeatherPredictionServiceImpl(HttpClientService httpClientService) {
		this.httpClientService = httpClientService;
	}

	/**
	 * It will call http request and get weather data and filter data for tomarrows
	 * date and return list with tomarrow forecasted weather data. It will throw
	 * WeatherPredictionReportNotFoundException exception if there is no data for
	 * zipcode.
	 * @param zipCode
	 * @return Forecastday List
	 */
	public List<Forecastday> getNextDayWeatherPrediction(String zipCode) {

		LocalDate today = LocalDate.now();
		LocalDate tommarow = today.plusDays(1);

		List<Forecastday> tommarowWeatherReport = new ArrayList<>();

		try {
			ResponseCodeAndBody responseCodeAndBody = httpClientService.get(ApiBuilder.getRequestURL(zipCode));

			WeatherReportModel weatherReport = new Gson().fromJson(responseCodeAndBody.getBody(),
					WeatherReportModel.class);

			if (weatherReport != null && !weatherReport.getForecast().getForecastday().isEmpty()) {
				logger.info("Checking the weather for {} at  {}  ({})", tommarow, zipCode,
						weatherReport.getLocation().getName());

				tommarowWeatherReport = weatherReport.getForecast().getForecastday().stream().filter(
						e -> e.date.equals(Date.from(tommarow.atStartOfDay(ZoneId.systemDefault()).toInstant())))
						.collect(Collectors.toList());

			} else {
				throw new WeatherPredictionReportNotFoundException(
						"The service was unable to fetch relevant weather prediction data for given region");

			}

		} catch (IOException e) {
			logger.info("Exception message: {}" ,e.getMessage());
		}

		return tommarowWeatherReport;

	}

}
