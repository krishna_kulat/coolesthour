package com.assignment.services;

import java.util.List;

import com.assignment.model.Forecastday;

public interface WeatherPredictionService {
	List<Forecastday>  getNextDayWeatherPrediction(String zipCode);
}
