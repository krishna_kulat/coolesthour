package com.assignment.services;

import java.util.List;
import com.assignment.model.Forecastday;
import com.assignment.model.ResponseCodeAndBody;

public interface WeatherTempratureService {
	ResponseCodeAndBody calculateAndPrintCoolestTemprature(List<Forecastday> forecastday);
}
