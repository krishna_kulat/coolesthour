package com.assignment.model;

import java.util.ArrayList;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Forecastday{
@JsonFormat(pattern="yyyy-MM-dd")
 public Date date;
 public int date_epoch;
 public Day day;
 public Astro astro;
 public ArrayList<Hour> hour;
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public int getDate_epoch() {
	return date_epoch;
}
public void setDate_epoch(int date_epoch) {
	this.date_epoch = date_epoch;
}
public Day getDay() {
	return day;
}
public void setDay(Day day) {
	this.day = day;
}
public Astro getAstro() {
	return astro;
}
public void setAstro(Astro astro) {
	this.astro = astro;
}
public ArrayList<Hour> getHours() {
	return hour;
}
public void setHours(ArrayList<Hour> hour) {
	this.hour = hour;
}
 
}