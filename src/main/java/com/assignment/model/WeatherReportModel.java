package com.assignment.model;

//import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
//import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString), Root.class); */

public class WeatherReportModel{
 public Location location;
 public Current current;
 public Forecast forecast;
public Location getLocation() {
	return location;
}
public void setLocation(Location location) {
	this.location = location;
}
public Current getCurrent() {
	return current;
}
public void setCurrent(Current current) {
	this.current = current;
}
public Forecast getForecast() {
	return forecast;
}
public void setForecast(Forecast forecast) {
	this.forecast = forecast;
}
 
}

