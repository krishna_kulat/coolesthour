package com.assignment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 
 * @author krishnakulat 
 * This class is used to handle exceptions
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

	@ExceptionHandler(RemoteServiceExecutionException.class)
	public ResponseEntity<Object> message(RemoteServiceExecutionException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(WeatherPredictionReportNotFoundException.class)
	public ResponseEntity<Object> message(WeatherPredictionReportNotFoundException e) {
		return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
