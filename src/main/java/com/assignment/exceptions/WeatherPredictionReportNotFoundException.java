package com.assignment.exceptions;

/**
 * 
 * @author krishnakulat
 * This exception throw when weather prediction data is not found for given zipcode
 *
 */
public class WeatherPredictionReportNotFoundException extends RuntimeException {


    public WeatherPredictionReportNotFoundException(String message) {
        super(message);
    }

}