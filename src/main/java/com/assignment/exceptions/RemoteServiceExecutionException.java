package com.assignment.exceptions;

/**
 * 
 * @author krishnakulat
 * This exception throw when http request not return 200
 *
 */
public class RemoteServiceExecutionException extends RuntimeException {


    public RemoteServiceExecutionException(String message) {
        super(message);
    }

}