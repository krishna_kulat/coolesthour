package com.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
 
/**
 * This application design for return coolest hour of the day for US zipcode
 */

@SpringBootApplication 
@EnableAutoConfiguration
//@PropertySource(value={"classpath:application.properties"})
public class WeatherPredictionApplication {
 
	
	public static void main(String[] args) {
		SpringApplication.run(WeatherPredictionApplication.class, args);
	
	}
	 
}
