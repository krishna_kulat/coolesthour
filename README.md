# README #
 

### Story ###

* As a user running the application
* I can view tomorrow�s predicted temperatures for a given zip-code in the United States
* So that I know which will be the coolest hour of the day.
### Story Point ###

* Consideration of design, developement, unit testing and functional testing - 8 story points

### Framework ###

* Spring boot MVC

### Executable war file ###

* https://bitbucket.org/krishna_kulat/coolesthour/src/master/ReleaseBuild/assignment-0.0.1-SNAPSHOT.war

### Release Note ###

*  https://bitbucket.org/krishna_kulat/coolesthour/src/master/ReleaseBuild/ReleaseNote.docx

### Postman collection ###

* https://bitbucket.org/krishna_kulat/coolesthour/src/master/PostmanCollection/coolest_hour.postman_collection.json

### Instruction ###
* Min Java version 8 required

* Weather API used from http://api.weatherapi.com 

* Internet is required as application uses third party api call to get weather data

* Execute the jar and hit the postman collection 

* Call the http request using postman collection/ browser/ curl

* API Request - http://localhost:8080/coolesthour/get?zipcode=10001

* Curl Request - curl -i -H "Accept: application/json" 'localhost:8080/coolesthour/get?zipcode=10001'

* Detail execution instruction is provide in Release Note